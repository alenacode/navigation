let map = L.map('map', {
    center: [43.1056, 131.874],
    minZoom: 2,
    zoom: 13,
})

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    subdomains: ['a', 'b', 'c'],
}).addTo(map)

let icon = L.icon({
    iconUrl: 'images/pin.png',
    iconSize: [24, 25],
    iconAnchor: [9, 21],
    popupAnchor: [0, -14],
})

let sidebar = L.control.sidebar({
    closeButton: true,
    container: 'sidebar',
    position: 'left',
}).addTo(map);

let panel = {
    id: 'point_info',              
    title: 'Данные о точке', 
    tab: '<i class="fa fa-times"></i>',
    pane: ' ',
}

sidebar.addPanel(panel);
for (let i = 0; i < markers.length; ++i) { 
    let marker; 
    if (markers[i]?.latlngs) {
        marker = L.polygon(markers[i].latlngs, {color: 'red'});            
    } else {
        marker = L.marker([markers[i].lat, markers[i].lng], { icon: icon });            
    }
    marker?.addTo(map).on('click', () => { setPanelContent(markers[i].data, markers[i].fileName) });
}

function setPanelContent(content, fileName) {
    let contentContainer = document.getElementsByClassName('leaflet-sidebar-content')[0];
    let pane = contentContainer.getElementsByClassName('leaflet-sidebar-pane')[0];
    pane.innerHTML = '<h1 class="leaflet-sidebar-header">Данные о точке<span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span></h1><p>' + content + '</p>'
    if (fileName) {
        pane.innerHTML += '<br/><a href="files/' + fileName + '" target="blank">Ссылка на документацию</a>';            
    }
    sidebar.open('point_info');
}