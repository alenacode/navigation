var markers = [
    {
        name: 'Какое-то здание',
        lat: 43.092593,
        lng: 131.864696,
        data: 'Какая-то информация о здании',
        fileName: 'file1.png',
    },
    {
        name: 'Какая-то зона',
        latlngs: [[43.085925, 131.854568], 
                  [43.086177, 131.848388], 
                  [43.083408, 131.851135], 
                  [43.085296, 131.857315]],
        data: 'Какая-то информация о зоне',
        fileName: 'README.md',
    },
]
